﻿namespace DXGridSaveLayoutSample.Model.Sale
{
   public class SaleInvoiceDetail
    {
        public long? DetailID { get; set; }
        public string ItemCode { get; set; }
        public decimal? Price { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Total { get; set; }
    }
}
