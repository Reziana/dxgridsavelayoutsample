﻿using System;
using System.Collections.ObjectModel;

namespace DXGridSaveLayoutSample.Model.Sale
{
   public class SaleInvoiceHeader
    {
        public long?  ID { get; set; }
        public DateTime?  Date { get; set; }
        public string  Note { get; set; }
        public ObservableCollection<SaleInvoiceDetail> Details { get; set; } = new ObservableCollection<SaleInvoiceDetail>();
    }
}
