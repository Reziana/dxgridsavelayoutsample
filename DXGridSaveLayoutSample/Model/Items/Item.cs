﻿
namespace DXGridSaveLayoutSample.Model.Item
{
    public class Item
    {
        public long   ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
        public decimal? Price { get; set; }
        public decimal? Cost { get; set; }
    }
}
