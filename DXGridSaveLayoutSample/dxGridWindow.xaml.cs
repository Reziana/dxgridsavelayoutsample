﻿using DevExpress.Xpf.Grid;
using DXGridSaveLayoutSample.Model.Sale;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DXGridSaveLayoutSample
{
    /// <summary>
    /// Interaction logic for dxGridWindow.xaml
    /// </summary>
    public partial class dxGridWindow : Window
    {

        private SaleInvoiceHeader DOC = new SaleInvoiceHeader() { Date = DateTime.Now, ID = 1, Note = "Test Document" };

        public dxGridWindow()
        {
            InitializeComponent();
            InitData();
        }

        private void InitData()
        {
            for (int i = 1; i < 4; i++)
            {
                DOC.Details.Add(new SaleInvoiceDetail() { DetailID = i, ItemCode = $"Code{i}", Price = 2 * i, Quantity = i, Total = (2 * i) * i });
            }
            _test.ItemsSource = Data.ItemList;
            this.DataContext = DOC;
        }


        private void PART_GridControl_Loaded(object sender, RoutedEventArgs e)
        {
            var gc = (GridControl)sender;

            SystemClass.LoadGridLayout(ref gc, "ItemsGridLayout");
        }

        private void PART_GridControl_Unloaded(object sender, RoutedEventArgs e)
        {
            SystemClass.SaveGridLayout((GridControl)sender, "ItemsGridLayout");
        }




        private void _dgItemsList_Loaded(object sender, RoutedEventArgs e)
        {
            SystemClass.LoadGridLayout(ref _dgItemsList, "DetailsGridLayout");
        }

        private void _dgItemsList_Unloaded(object sender, RoutedEventArgs e)
        {
            SystemClass.SaveGridLayout((GridControl)sender, "DetailsGridLayout");
        }
    }




    class SystemClass
    {




        public static string APP_PATH
        {
            get
            {
                return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            }
        }



        public static string PATH_ELIF_DATA_FOLDER
        {
            get { return $"{APP_PATH}\\localData\\"; }
        }


        private static void CheckDirectory()
        {
            if (!Directory.Exists(PATH_ELIF_DATA_FOLDER))
            {
                Directory.CreateDirectory(PATH_ELIF_DATA_FOLDER);
            }
        }


        public static void LoadGridLayout(ref GridControl grid, string fn)
        {
            try
            {
                if (string.IsNullOrEmpty(fn)) return;

                var fpath = $"{PATH_ELIF_DATA_FOLDER}{fn}.xml";

                if (File.Exists(fpath))
                {
                    grid.RestoreLayoutFromXml(fpath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        public static void SaveGridLayout(GridControl grid, string fn)
        {
            try
            {
                if (string.IsNullOrEmpty(fn)) return;

                CheckDirectory();

                var fpath = $"{PATH_ELIF_DATA_FOLDER}{fn}.xml";


                grid.SaveLayoutToXml(fpath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }




}
