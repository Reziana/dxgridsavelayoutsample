﻿using DXGridSaveLayoutSample.Model.Item;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXGridSaveLayoutSample
{
  public static class Data
  {
      public static ObservableCollection<Item> ItemList = new ObservableCollection<Item>()
      {
          new Item(){ ID=1 , Code="Code1" , Description="Item1",Unit="Unit1", Cost=1 , Price=2},
          new Item(){ ID=2 , Code="Code2" , Description="Item2",Unit="Unit2", Cost=2 , Price=3},
          new Item(){ ID=3 , Code="Code3" , Description="Item3",Unit="Unit3", Cost=3 , Price=4},
          new Item(){ ID=4 , Code="Code4" , Description="Item4",Unit="Unit4", Cost=4 , Price=5},
          new Item(){ ID=5 , Code="Code5" , Description="Item5",Unit="Unit5", Cost=5 , Price=6},
          new Item(){ ID=6 , Code="Code6" , Description="Item6",Unit="Unit6", Cost=6 , Price=7},
          new Item(){ ID=7 , Code="Code7" , Description="Item7",Unit="Unit7", Cost=7 , Price=8},
          new Item(){ ID=8 , Code="Code8" , Description="Item8",Unit="Unit8", Cost=8 , Price=9},
          new Item(){ ID=9 , Code="Code9" , Description="Item9",Unit="Unit9", Cost=9 , Price=10},
      };


 
  }
}
